export class Breakout extends Phaser.Game {
	constructor() {
		//log phaser version to the console
		console.log("Hello, World! from Phaser-" + Phaser.VERSION);

		//set game config
		super(720, 1280, Phaser.CANVAS, "content");

		this.state.add('Boot', Boot, false);
		this.state.add('Preload', Preload, false);
		this.state.add('Start', Start, false);
		this.state.add('Level', Level, false);
		this.state.add('GameOver', GameOver, false);
		this.state.add('Win', Win, false);

		this.state.start('Boot');
	}
}

class Boot extends Phaser.State {
	create(){
		this.game.state.start('Preload');
	}
}

//============= Declare global scope sound variables ============
var snd_music: any;
var snd_bounce1: any;
var snd_bounce2: any;
var snd_bouncebeep1: any;
var snd_bouncebeep2: any;
var snd_chain: any;
var snd_crumble1: any;
var snd_crumble2: any;
var snd_crumble3: any;
var snd_start: any;
var snd_lose: any;
var snd_win: any;
var snd_newCell: any;

class Preload extends Phaser.State {
	preload(){
		//load resources
		this.load.image('ball', 'assets/ball.png');
		this.load.image('openingText', 'assets/Tap_to_restart.png');
		this.load.image('leftWall', 'assets/left_wall.png');
		this.load.image('rightWall', 'assets/right_wall.png');
		this.load.spritesheet('brick', 'assets/bricksprites.png',100,125.3);

		//particle images
		this.load.image('breakPart1', 'assets/break_part1.png');
		this.load.image('breakPart2', 'assets/break_part2.png');
		this.load.image('breakPart3', 'assets/break_part3.png');

		//fonts
		this.load.bitmapFont('montserratBold', 'assets/montserratBold.png', 'assets/montserratBold.fnt');
		this.load.bitmapFont('montserrat', 'assets/montserrat.png', 'assets/montserrat.fnt');

		//audio
		this.load.audio('loopingSoundtrack', 'assets/audio/looping_soundtrack.ogg');
		this.load.audio('bounce1', 'assets/audio/bounce.ogg');
		this.load.audio('bounce2', 'assets/audio/bounce2.ogg');
		this.load.audio('bouncebeep1', 'assets/audio/bouncebeep.ogg');
		this.load.audio('bouncebeep2', 'assets/audio/bouncebeep2.ogg');
		this.load.audio('chain', 'assets/audio/chain.ogg');
		this.load.audio('crumble1', 'assets/audio/crumble1.ogg');
		this.load.audio('crumble2', 'assets/audio/crumble2.ogg');
		this.load.audio('crumble3', 'assets/audio/crumble3.ogg');
		this.load.audio('start', 'assets/audio/start.ogg');
		this.load.audio('lose', 'assets/audio/lose.ogg');
		this.load.audio('win', 'assets/audio/win.ogg');
		this.load.audio('newCell', 'assets/audio/new_cell.ogg');
	}

	create(){
		//set up physics
		this.game.physics.startSystem(Phaser.Physics.ARCADE);
		this.game.physics.arcade.setBounds(wallWidth,0,gameWidth -wallWidth*2,gameHeight+100);
		this.game.forceSingleUpdate = false;
		this.game.time.desiredFps = 50;

	  // set up canvas
	  this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
	  this.stage.backgroundColor = '#1D1D1D';
		this.scale.setResizeCallback(screenResize, this);

		//set up object groups
		worldObjects = this.game.add.group();
		uiObjects = this.game.add.group();

		//create music audio sprite
		snd_music = this.add.audio('loopingSoundtrack');
		snd_music.volume = 0.2;
		snd_music.loop = true;
		snd_music.play();

		//create sound fx audio sprites
		snd_bounce1 = this.add.audio('bounce1');
		snd_bounce1.volume = 0.1;
		snd_bounce2 = this.add.audio('bounce2');
		snd_bounce2.volume = 0.1;
		snd_bouncebeep1 = this.add.audio('bouncebeep1');
		snd_bouncebeep1.volume = 0.3;
		snd_bouncebeep2 = this.add.audio('bouncebeep2');
		snd_bouncebeep2.volume = 0.3;
		snd_chain = this.add.audio('chain');
		snd_chain.volume = 0.3;
		snd_crumble1 = this.add.audio('crumble1');
		snd_crumble2 = this.add.audio('crumble2');
		snd_crumble3 = this.add.audio('crumble3');
		snd_start = this.add.audio('start');
		snd_start.volume = 0.5;
		snd_lose = this.add.audio('lose');
		snd_lose.volume = 0.5;
		snd_win = this.add.audio('win');
		snd_win.volume = 0.4;
		snd_newCell = this.add.audio('newCell');
		snd_newCell.volume = 0.3;

		this.game.state.start('Start', false);
	}
}

//========== Declare important global scope vars ===========
var worldObjects: any;
var uiObjects: any;
var score: number;

var gameWidth: number = 720;
var gameHeight: number = 1280;
var wallWidth: number = 35;
var collumnNum: number = 6;
var brickTypes: number = 4; //defines the range of brick colours available
var colorShift: number = Math.random();

var ball: any;
var paddle: any;
var bricks: any;
var lWall: any;
var rWall: any;

var scoreText: any;
var messageText: any;
var messageText2: any;

class Start extends Phaser.State {

	create(){

		//create brick group
		bricks = this.add.group();
		worldObjects.add(bricks);
		bricks.classType = Brick;
		bricks.enableBody = true;
		bricks.physicsBodyType = Phaser.Physics.ARCADE;

		//create wall sprites
		lWall = this.add.sprite(0,gameHeight,'leftWall');
		worldObjects.add(lWall);
		lWall.anchor.setTo(0,1);

		rWall = this.add.sprite(gameWidth,gameHeight,'rightWall');
		worldObjects.add(rWall);
		rWall.anchor.setTo(1,1);

		//create score text object
	  scoreText = this.add.bitmapText(gameWidth/2, 110, 'montserratBold','',100);
		scoreText.anchor.setTo(0.5,0.5);
		scoreText.alpha = 1;
		uiObjects.add(scoreText);

		//create message text object
	  messageText = this.add.bitmapText(gameWidth/2, gameHeight/2.4, 'montserrat','Tap the screen to start',80);
		messageText.anchor.setTo(0.5,0.5);
		messageText.align = 'center';
		messageText.alpha = 1;
		messageText.maxWidth = gameWidth;
		uiObjects.add(messageText);

		//create small message text object
	  messageText2 = this.add.bitmapText(gameWidth/2, gameHeight/2.4 +70, 'montserrat','',30);
		messageText2.anchor.setTo(0.5,0.5);
		messageText2.align = 'center';
		messageText2.alpha = 1;
		messageText2.maxWidth = gameWidth;
		uiObjects.add(messageText2);

		//add mouse press listener
		this.input.onDown.add(restartGame, this);
	}
}

class Level extends Phaser.State {
	create(){
		colorShift = Math.random();
		let color = make_color_hsl(colorShift, 1, 0.8);

		//create grid of bricks
		bricks.removeAll(true);
		if (bricks.length == 0){
			this.createBricks(0,4,collumnNum,6);
		}

		//reset all the bricks
		bricks.forEachExists(this.resetBricks,this);

		if (paddle != null){
			paddle.destroy();
			paddle = null;
		}
		//create paddle
		paddle = new Paddle(this.game, this.game.world.centerX, gameHeight -170, 6);
		worldObjects.add(paddle);

		if (ball != null){
			ball.destroy();
			ball = null;
		}
		//create ball
		ball = new Ball(this.game,100,100);
		worldObjects.add(ball);
		ball.x = this.game.world.centerX;
		ball.y = this.game.world.centerY;
		ball.body.velocity.y = 600;

		//reset score
		score = 0;
		scoreText.text = score;

		//play start sound
		snd_start.play();

		//reset messages
		messageText.text = '';
		messageText2.text = '';
	}

	createBricks(x,y,width,height,pattern?){
		//create a set of bricks within a region of the grid
		for (let yy = y; yy < y +height; yy ++){
			for (let xx = x; xx < x +width; xx ++){
				bricks.create(xx,yy);
			}
		}
		bricks.sort('y', Phaser.Group.SORT_DESCENDING);
	}

	resetBricks(brick){
		//reset all the bricks
		brick.revive();
		brick.setBrickProperties();
	}
}

class GameOver extends Phaser.State {
	create(){
		//play gameover sound
		snd_lose.play();

		//display the game over message
		messageText.text = 'GAME OVER';
		messageText2.text = 'Tap to restart';

		//add mouse press listener
		this.input.onDown.add(restartGame, this);
	}
}

class Win extends Phaser.State {
	create(){
		//play win sound
		snd_win.play();

		scoreText.text = '';
		messageText.text = 'Congratulations!';
		messageText2.text = 'You scored: '+score;

		//add mouse press listener
		this.input.onDown.add(restartGame, this);
	}
}

function screenResize(scale, parent){
	//update the game's aspect ratio to cover the window (locking the x dimention)
	let aspect = window.innerHeight /window.innerWidth;
	gameHeight = gameWidth *aspect;
	this.scale.setGameSize(gameWidth, gameHeight);
	this.physics.arcade.setBounds(wallWidth,0,gameWidth -wallWidth*2,gameHeight+100);

	//move wall sprites
	if (lWall != null){
		lWall.y = gameHeight;
		rWall.y = gameHeight;
	}

	//position text
	if (messageText != null){
		messageText.y = gameHeight/2.4;
		messageText2.y = gameHeight/2.4 +70;
	}
}

function restartGame(){
	//reset the game to the level state
	this.game.state.start('Level', false);
}

function make_color_hsl(h,s,l){
	//create a color with hsl and return an integer
	let colorRGB = Phaser.Color.HSLtoRGB(h, s, l);
	return Phaser.Color.getColor(colorRGB.r, colorRGB.g, colorRGB.b);
}

class Ball extends Phaser.Sprite{
	ballType: number;

	constructor(game,x,y){
		super(game, x, y,'ball');
		//add to the world
		game.add.existing(this);

		let size = 30;
		this.anchor.setTo(0.5, 0.5);
		game.physics.enable(this, Phaser.Physics.ARCADE);
		this.body.setSize(size,size,this.offsetX -size/2,this.offsetY -size/2);
		this.body.collideWorldBounds = true;
		this.body.bounce.set(1);
		this.body.onCollide = new Phaser.Signal();
		this.body.onCollide.add(this.onCollide, this);
		this.body.onWorldBounds = new Phaser.Signal();
		this.body.onWorldBounds.add(this.onWorldBounds, this);

		//set ball color
		this.ballType = Math.floor(Math.random()*brickTypes);
		this.tint = make_color_hsl(colorShift +this.ballType *1/brickTypes, 1, 0.8);
	}

	update(){
		//check collisions
		this.game.physics.arcade.collide(this, bricks, this.collide_brick, null, this);
		this.game.physics.arcade.collide(this, paddle, this.collide_paddle, null, this);

		//when the ball leaves the world, change to game over state
		if (this.y > gameHeight && this.game.state.current == 'Level'){
			this.game.state.start('GameOver',false);
			this.ball_pop();
		}
	}

	collide_paddle(coll_ball,coll_paddle){
		//modify the balls horizontal velocity
		this.body.velocity.x += (0 -this.body.velocity.x) *0.6;
		this.body.velocity.x += (this.x -paddle.x)/paddle.cells.length *20 +Math.pow(paddle.body.velocity.x*0.01,3)*0.05;
		if (Math.round(this.body.velocity.x) == 0){
			this.body.velocity.x = -100 +Math.random()*200;
		}
		this.body.velocity.x = Phaser.Math.clamp(this.body.velocity.x,-1200,1200);

		//determine the paddle cell that the ball collided with
		let cell = Phaser.Math.clamp(Math.floor((this.x -(paddle.x -paddle.size.x/2))/paddle.cellSize), 0, paddle.cells.length -1);

		//mobify the paddle cells
		if (paddle.cells[cell] == this.ballType){
			//if cell color is the same as the ball color, add a new cell of the same color
			paddle.add_cell(cell,this.ballType);

			//play new cell sound
			snd_newCell.play();

			//if the paddle exceeds 8 then delete a cell on the opposite side of the paddle
			if (paddle.cells.length > 8 && paddle.cellDelete == -1){
				if (cell > 4){
					paddle.delete_cell(0);
				}else{
					paddle.delete_cell(paddle.cells.length-1);
				}
			}
		}else if (this.ballType != -1){
			//if the ball color is different to the cell color, replace the cell with the ball color
			paddle.delete_cell(cell,true);
			paddle.add_cell(cell,this.ballType);

		}else{
			//if the ball is white, delete the cell
			paddle.delete_cell(cell,true);
			paddle.find_chain(cell);

			if (paddle.cells.length == 0){
				paddle.destroy();
			}
		}

		//reset ball color
		this.ballType = -1;
		let color = make_color_hsl(0, 0, 1);
		this.tint = color;
	}

	collide_brick(coll_ball,coll_brick){
		//increase the score when the ball hits a brick
		score += 5;
		scoreText.text = score;

		this.game.rnd.pick([snd_bouncebeep1,snd_bouncebeep2]).play();

		//set ball color to that of the brick
		this.ballType = coll_brick.brickType;
		if (this.ballType == -1){
			var color = make_color_hsl(0, 0, 1);
		}else{
			var color = make_color_hsl(colorShift +this.ballType *1/brickTypes, 1, 0.8);
		}
		this.tint = color;

		coll_brick.kill();

		//if no more bricks remain then change to win state
		if (bricks.countLiving() == 0){
			this.game.state.start('Win',false);
		}
	}

	onCollide(){
		//play ball bounce sound when hitting objects
		this.game.rnd.pick([snd_bounce1,snd_bounce2]).play();
	}

	onWorldBounds(){
		//play ball bounce sound when hitting walls
		this.game.rnd.pick([snd_bounce1,snd_bounce2]).play();
	}

	ball_pop(){
		//destroy the ball object
		this.destroy();
		ball = null;
	}
}

class Paddle extends Phaser.Graphics{
	//Defines the paddle
	size: any;
	shapeMask: any;
	dragPointer: any;
	pointerPrev: any;
	cellSize: number;
	cells: number[];
	cellDelete: number;
	cellDeleteTimeout: number;
	partEmitter: any;
	partEmitterType: number;

	constructor(game,x,y,cellNum){
		super(game, x, y);

		//add to the world
		game.add.existing(this);

		//activate resize function manually for repositioning
		this.game.scale.onSizeChange.add(this.resize,this);

		//define the paddle cells array
		this.cellSize = 40;
		this.cells = [];
		for (let i = 0; i < cellNum; i++){
			let cellType = Math.floor(Math.random()*brickTypes);
			this.cells.push(cellType);
		}
		this.cellDelete = -1;

		this.size = new Phaser.Point(this.cellSize*cellNum, this.cellSize);

		//create a mask of the paddle shape
		this.shapeMask = this.addChild(game.make.graphics(-this.size.x/2, -this.size.y/2));

		//apply roundrect mask
		this.mask = this.shapeMask;

		//enable physics body
		game.physics.enable(this, Phaser.Physics.ARCADE);
		this.body.setSize(this.size.x,this.size.y,-this.size.x/2,-this.size.y/2);
		this.body.collideWorldBounds = true;
		this.body.immovable = true;

		//draw the cells that make the paddle
		this.update_cells();

		//remove any cell chains
		this.find_chain();

		//update position when pointer moves
		this.dragPointer = game.input.activePointer;
		this.pointerPrev = new Phaser.Point(this.dragPointer.x, this.dragPointer.y);
		game.input.onDown.add(this.mousePressed,this)

		//add particle emitter
		this.partEmitter = game.add.emitter(0, 0, 60);
		this.partEmitter.particleClass = CellPart;
    this.partEmitter.makeParticles(['breakPart1','breakPart2','breakPart3']);
		this.partEmitter.minParticleScale = 0.5;
		this.partEmitter.maxParticleScale = 1;
    this.partEmitter.gravity = 500;
	}

	mousePressed(){
		this.pointerPrev.x = this.dragPointer.x;
	}

	update(){
		//drag paddle with mouse
		if (this.dragPointer.isDown){
			this.body.velocity.x += ((this.dragPointer.x -this.pointerPrev.x)*90 -this.body.velocity.x)/3;
		}else{
			this.body.velocity.x += (0 -this.body.velocity.x)/10;
		}
		this.pointerPrev.x = this.dragPointer.x;


		//delete cells in queue one by one
		if (this.cellDeleteTimeout >= 5){
			if (this.cellDelete != -1){
				//delete the next cell in the queue
				let cellType = this.cells[this.cellDelete];
				this.delete_cell(this.cellDelete);

				let adjacentCells = [];
				if (this.cellDelete < paddle.cells.length && this.cells[this.cellDelete] == cellType){
					adjacentCells.push(this.cellDelete);
				}
				if (this.cellDelete-1 >= 0 && this.cells[this.cellDelete-1] == cellType){
					adjacentCells.push(this.cellDelete-1);
				}

				if (adjacentCells.length > 0){
					this.cellDelete = this.game.rnd.pick(adjacentCells);
				}else{
					//test if new chain has formed
					this.find_chain(this.cellDelete);
				}

				this.cellDeleteTimeout = 0;
			}
		}else{
			this.cellDeleteTimeout ++;
		}

	}

	update_cells(){
		//draw the cells that make the paddle
		this.clear();
		this.game.rnd.sow([2839]);
		for (let i = 0; i < this.cells.length; i ++){
			//set cell color
			let color = make_color_hsl(colorShift +this.cells[i] * 1/brickTypes, 0.35 +this.game.rnd.frac()*0.2, 0.5 +this.game.rnd.frac()*0.1);

			this.beginFill(color);
			this.drawRect(-this.size.x/2 +i*this.cellSize,-this.size.y/2,this.cellSize,this.cellSize);
			this.endFill();
		}

		//update the rounded rectangle mask to fit the paddle size
		this.shapeMask.clear();
		this.shapeMask.x = -this.size.x/2;
		this.shapeMask.y = -this.size.y/2;
		this.shapeMask.beginFill(0xffffff);
		this.shapeMask.drawRoundedRect(0, 0, this.size.x, this.size.y,15);
		this.shapeMask.endFill();

		//update collision box size
		this.body.setSize(this.size.x,this.size.y,-this.size.x/2,-this.size.y/2);
	}

	delete_cell(pos,chain = false){
		//start a particle burst using the partEmitter
		this.partEmitter.x = this.x -this.size.x/2 +this.cellSize/2 +pos *this.cellSize;
		this.partEmitter.y = this.y;
		this.partEmitterType = this.cells[pos];
		this.partEmitter.start(true, 3000, null, 5);

		//play random cell break sound
		this.game.rnd.pick([snd_crumble1,snd_crumble2,snd_crumble3]).play();

		//delete the color cell
		this.cells.splice(pos, 1);
		this.size.setTo(this.cellSize*this.cells.length, this.cellSize);
		this.update_cells();
	}

	add_cell(pos,type){
		//add a cell and check if a chain was made
		this.cells.splice(pos, 0, type);
		this.size.setTo(this.cellSize*this.cells.length, this.cellSize);
		this.update_cells();

		this.find_chain(pos);
	}

	find_chain(pos = 0){
		//finds a chain of matching cell colors on the paddle
		let minChainLength = 4;

		let chainStart = pos;
		let chainLength = 1;

		//find the start of cell chain
		while (chainStart-1 >= 0 && paddle.cells[chainStart-1] == paddle.cells[pos]){
			chainStart --;
		}
		//find the length of the cell chain
		while (chainLength < minChainLength && chainStart < this.cells.length-(minChainLength-1)){
			chainLength = 1;
			while (chainStart+chainLength < this.cells.length+1 && this.cells[chainStart+chainLength] == this.cells[chainStart]){
				chainLength ++;
			}
			if (chainLength < minChainLength){
				chainStart = chainStart +chainLength;
			}
		}

		if (chainLength < minChainLength){
			//if no chain is found then stop deleting cells
			this.cellDelete = -1;
		}else{
			//if a chain was found, add to the players score depending on the chain length
			score += 70 +80*(chainLength-minChainLength)*0.25;
			scoreText.text = score;

			//play chain sound
			snd_chain.play();

			//start deleting cells from the point that was hit or the start of the chain
			if (pos != 0){
				this.cellDelete = pos;
			}else{
				this.cellDelete = chainStart;
			}
			this.cellDeleteTimeout = 0;
		}
	}

	resize(){
		//reposition when screen size changes
		this.y = gameHeight -170;
	}
}

class CellPart extends Phaser.Particle{
	//define a custon particle class for cell breaking to add custom tinting
	constructor(game,x,y,key,frame){
		super(game,x,y,key,frame);
		this.anchor.setTo(0.5,0.5);
	}

	onEmit(){
		this.tint = make_color_hsl(colorShift +paddle.partEmitterType * 1/brickTypes, 0.35 +this.game.rnd.frac()*0.2, 0.45 +this.game.rnd.frac()*0.2);
	}
}

class Brick extends Phaser.Sprite{
	//Define a brick class
	brickType: number;
	gridX: number;
	gridY: number;
	size: any;

	constructor(game,gridX,gridY){
		let columnWidth = (gameWidth -wallWidth*2)/collumnNum;
		super(game,wallWidth+columnWidth/2 +gridX *columnWidth, gridY *(40 +10),'brick',0);

		this.size = new Phaser.Point(100,40);
		this.gridX = gridX;
		this.gridY = gridY;

		this.setBrickProperties();

		game.physics.enable(this, Phaser.Physics.ARCADE);
		this.body.setSize( this.size.x, this.size.y, 0, this.texture.height -this.size.y);
		this.body.immovable = true;

		//***Old code for creating dynamic graphic (abandoned due to mobile performance issues)***
		//create a light tail behind the brick graphic
		/*let tail = game.make.sprite(margin,size.y/2,'brickTail');
		tail.anchor.setTo(0,1);
		tail.scale.x = (size.x -margin*2)/100;
		tail.tint = color;
		this.addChild(tail);

		//create the brick fill color
		let brickRect = game.make.graphics();
		brickRect.beginFill(color);
		brickRect.drawRect(margin,margin,size.x -margin*2,size.y -margin*2);
		brickRect.endFill();
		this.addChild(brickRect);

		//create a mask of the brick shape
		let shapeMask = game.make.graphics();
		shapeMask.beginFill(color);
		shapeMask.drawRoundedRect(this.x +margin,this.y +margin,size.x -margin*2,size.y -margin*2,14);
		shapeMask.endFill();

		brickRect.mask = shapeMask;*/
	}

	setBrickProperties(){
		//select brick color based on grid position
		let color;
		if (Math.floor(Math.random()*6) == 0){
			this.brickType = -1;
			color = make_color_hsl(0, 0, 0.85);
			this.frame = 0;
		}else{
			this.brickType = this.gridY % brickTypes;
			color = make_color_hsl(colorShift +this.brickType * 1/brickTypes, 0.6 +Math.random()*0.1, 0.55 +Math.random()*0.1);
			this.frame = this.brickType +1;
		}

		//position and color sprite
		this.anchor.setTo(0.5, (1/this.texture.height) * (this.texture.height -this.size.y/2));
		this.tint = color;
	}
}
